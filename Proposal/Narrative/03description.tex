\section{EXASCALE SOFTWARE DESCRIPTION}\label{sec:3}

\subsection{SOFTWARE OVERVIEW AND STATUS}\label{sec:3.1}
%% \guidance{What functionality does the software provide, and what is its current 
%% status? Which exascale applications or other components of the exascale software 
%% stack use it currently, or are expected to use it, and how? Clearly state the 
%% issues that are addressed by this software technology at either the application 
%% level or within the overall software stack.
%Describe the level of effort currently funded for this software development 
%and/or its ongoing maintenance. For brevity, include references to other 
%sources, links, etc., where details about the software have already been 
%described.
%}

PETSc/TAO,
\href{http://www.mcs.anl.gov/petsc}{http://www.mcs.anl.gov/petsc} is a
mature software library for the scalable solution of linear,
nonlinear, and ODE/DAE systems, computation of adjoints (sometimes
called sensitivities) of ODE/PDE systems, and optimization. In the
software stack PETSc fits above the system software, programming
models and tools, and directly below the application codes.


EnsembleLib is a new library that will provide scheduling, control,
and monitoring capabilities for running many simultaneous simulations.
Algorithms can be built on top of EnsembleLib for numerical
optimization, sensitivity analysis, and uncertainty quantification;
our focus in the proposed work is on numerical optimization methods,
but in future years we will pursue funding, from a variety of sources,
for utilizing EnsembleLib for SA and UQ.  A key to this work is being
able to partition communicators and reuse them when a simulation is
either not converging or converging to a solution that does not
provide useful information.  Advanced controls, such as setting the
required precision of the simulations and allowing the dynamic growth
of the pool of simulation to, for example, improve the accuracy of a
stochastic quantity of interest, complicate the design and
implementation, yet such controls will eventually be critical for improving the performance of the
numerical optimization algorithms. Though EnsembleLib will be
developed as a standalone library, it will utilize the same software
development processes and testing as are already used for PETSc/TAO;
see Section 6.2
 
The PETSc/TAO current funding
support is discussed in Primary Funding Sources in Section 1.
 
 
\subsection{SOFTWARE DEVELOPMENT NEEDS}
\label{sec:softdev}
%% \guidance{Briefly describe the specific research and development results that 
%% would help applications (or the facility software infrastructure) use 
%% productively the projected exascale systems or enable applications with new 
%% workflows that require exascale. Where relevant, address the following topics.}

PETSc will provide efficient algebraic solvers, time-integrators and adjoint computations needed by the exascale applications. 
The numerical optimization capabilities built on top of EnsembleLib will 
enable 
applications to perform parameter estimation and design optimization fully utilizing 
the exascale resources. Some of the research results we will incorporate into our work include the approaches in 
\cite{balasubramanian2016ensemble, merzky2016executing, wozniak2013jets,
elsheikh2013iterative, elsheikh2013boosting, nino2015parallel, LarWild14, LW16, 
HOPSPACK}.
While not our primary focus, sensitivity analysis 
and uncertainty quantification methods would also become available. We will develop two key demos using EnsembleLib:

1.  Computation of multiple local minimizers and local sensitivities

Many parameter estimation and design optimization problems have goal functions that are typically nonlinear and nonconvex.  Therefore, our 
optimization methods built on EnsembleLib will be capable of producing many
high-quality local minimizers for the goal function specified along with
local sensitivities that will allow domain scientists to further 
evaluate these minimizers and refine their criteria for their models.  This is more responsive to the to the needs of the application
scientists than providing a single, non-unique and possibly not even physical minimizer.

2.  Stochastic optimization

Often, the parameters for the simulations are unknown and fall within a 
distribution,
leading to stochastic counterparts to the dynamic simulation.  To perform 
optimization
with these stochastic systems, we will extend our optimization solvers written 
using
EnsembleLib to provide a framework for stochastic optimization using sample
average approximation methods.  Key needs are refining the ensemble to
reduce the uncertainty in the objective functions.


\subsubsection{System Requirements}
%% \guidance{Identify any dependencies the software product has or will have with 
%% other components of the software stack. If relevant and available, discuss the 
%% performance achieved on current architectures for the proposed technology. 
%% Describe any specific hardware and software requirements for the proposed 
%% activities. Describe any assumptions made by the proposed code effort in any or 
%% all of the following areas, as appropriate: availability of testbed systems, 
%% software development environments or tools, software engineering support, 
%% exascale system readiness and timelines, application readiness, and so on.  What 
%% core capabilities are assumed will be in place for success of the proposed 
%% work?}

PETSc/TAO is written in portable C and seamlessly uses vendor
libraries (e.g., MKL) when available for Intel-based systems and
GPU-accelerated libraries (e.g., cusparse, CUSP, ViennaCL) as needed
for GPU utilization. This approach allows maximum code reuse since
applications can be written in Fortran, Python, C, or C++.  The only
hard dependencies we have are on a quality MPI implementation and
system software provided by Intel and Nvidia.  If
\href{http://www.argo-osr.org/}{Argo} work is supported by ECP, we
will utilize its APIs in portions of the thread neutral ``bypass MPI''
optimizations. PETSc attempts to be ``thread neutral'' and work
equally well with applications and other libraries that choose to use
OpenMP or other threading models and those that choose to be pure MPI
\cite{KnepleyBrownMcInnesSmithRuppAdams2015}. Thus, we are not dependent on a 
high-performance OpenMP
implementation but could utilize it if such an implementation were available.  
PETSc/TAO
runs on the current leadership computing facility (LCF) systems.
Its performance, like those of all
implicit solver packages, is dictated largely by the memory bandwidth
of the current systems. Consequently, PETSc/TAO and its applications will benefit from the high memory bandwidth of the Intel
Phi series and the memory performance improvements for the incoming Nvidia
GPU systems.


EnsembleLib requires certain improvements to MPI for monitoring and
stopping running simulations and recovering the resources so that they
can be reused.  These have been discussed with the MPICH development
group and require only incremental improvements to the MPI
infrastructure. We will work with the MPICH team to develop these
improvements.

Testbed systems will be needed for running the algorithms and software
at scale to detect and remove any surprising scalability issues. Node-level 
code development and testing will be done on local dedicated
systems. We have requested a small hardware budget to ensure that our
developers always have dedicated access and are not blocked by unavailable shared systems.

\subsubsection{Technology Maturity}
%% \guidance{Provide a self-assessment of the maturity of the software technology 
%% from the perspective of its readiness for production-level use by applications 
%% or system vendors. Identify technical gaps in the technology that are barriers 
%% to production use. Provide an estimate of the support model required in the long 
%% term for the software technology, if applicable (or a potential transition path 
%% to a vendor or ISV).}

PETSc/TAO is mature software and has been used at scale as well as on
smaller systems for over twenty years. It has a vigorous user
community and support infrastructure (see Section 6.2.1). The algorithmic
and software infrastructure it uses are well understood, and it has
received consistent support from DOE's ASCR programs. Cray has provided
a version of the PETSc libraries for a number of years with their
software toolkits. The ISV Tech-X is developing industrial quality improvements 
to the PETSc documentation and installation in collaboration with the PETSc team 
through a DOE ASCR SBIR project.

\subsubsection{Cross-Team Collaboration and Integration}
%% \guidance{For software technologies to be useful, it is essential that the teams 
%% engage in significant collaborations with other components of the software stack 
%% as well as with applications and co-design centers. Discuss this engagement in 
%% the proposed work, and describe any required supporting research, development 
%% and production-ready steps that must be taken. Identify interfaces to other 
%% software technologies that will be required. Include specific known application 
%% targets, required interactions with other software technologies outside the 
%% scope of the proposed effort, and include an integration plan (e.g., the 
%% software is already in use by applications, or include project team members from 
%% the applications and other software technology teams on the proposed team).
%% Integration and interdependencies of software development activities with other activities within the ECP, e.g., other components of the software stack, applications, hardware technologies, co-design centers, as well as deployment of exascale systems, will be needed for ECP’s success. Call out the critical needs from any of the following focus areas and also indicate the results/products that are expected to be delivered to any of these areas:
%% \begin{description}
%% \item[Application Development] ECP endeavors to enhance the predictive capability of selected DOE and other agency applications through requirements-based development of physical models, algorithms, and methods; integration of appropriate software and hardware co-design methodologies; systematic improvement of exascale system readiness and utilization; and demonstration and assessment of effective software/hardware integration.
%% \item[Software Technology] This ECP focus area will develop and deploy a comprehensive software stack that includes programming and system software, libraries and frameworks, tools, data management and workflows, data analytics and visualization, and resilience and integrity.
%% \item[Hardware Technology] The hardware technology focus area supports DOE-vendor collaborative R\&D activities required to develop and deploy at least two exascale systems with diverse architectural features in support of HPC exascale system acquisitions. The opportunity for software technology needs to influence hardware architecture designs is provided by the early start on vendor-led system and node design projects.
%% \item[Exascale Systems] The exascale systems focus area supports advanced system engineering development by vendors and the cost of system expansion needed to produce capable exascale systems. The focus area also includes acquisition and support of prototypes and test beds for the application, software, and hardware activities. Please identify relevant requirements for the acquisition of testbeds and the readiness of exascale systems that will enable success of your software development proposal.
%% \item[Other Programs] Where appropriate, include a brief description of the interdependency of the proposed work across other existing programs in the US Department of Energy or other federal agencies, as well as US academia and industry.
%% \end{description}}

All the ECP
application proposals listed in Table 2.1 have needs that could be satisfied by components of PETSc/TAO. Once the ECP
application groups have been selected we will contact them and begin
setting up appropriate collaborations for those that currently do not exist. The main system level software
projects we will collaborate with are the MPICH team (needed for the
EnsembleLib work) and the Argo project (useful for the node level
optimization work). The CODAR data reduction and analysis center will enable us to effectively 
reduce 
and store the data from the simulation so that we can use the output from the 
runs 
performed to approximate different objective functions and enable rapid 
exploration 
by the domain scientists.  Calculation of the objective function can also be 
performed 
online as the simulation runs (e.g., at the current time step, rather than at 
the 
end).
Such online objective function evaluations can be used to prune simulations 
that 
cannot improve over the incumbent objective function value.  Online monitoring
of the simulation can also tell when the simulation is not making progress 
and should be terminated. The proposed ECP co-design Center for
 Efficient Exascale Discretizations (CEED) will utilize our partially matrix-free solvers while we and the application teams will be able to utilize their implementation of higher order methods.
 

We are already involved in several proposed ECP applications and co-design
centers (see the current and pending materials for exact details).  In
particular co-PI Wild has extensive experience from multiple SciDAC
application partnerships in working with applications groups and
providing the bridge to optimization tools. We will use our embedded
personnel in applications to produce the numerical optimization
problems.  We will leverage our efforts in the data co-design centers
to improve the memory hierarchy performance and for the online data
analysis for computing the objective functions and adaptive data
reduction prior to storage so that the functions can be re-evaluated
when the objective changes. We have friendly relations with the
application simulation teams, some of whom currently utilize PETSc,
and will collaborate with them both in assisting them to utilize
PETSc/TAO and in utilizing their application codes to
determine and remove scalability bottlenecks in the solvers,
integrators, and optimization algorithms they utilize. PETSc solvers
have been used or are being used in most DOE fusion simulators, some
subsurface flow codes, materials simulators, and accelerator
simulators.

In order to facilitate interoperability with complementary
capabilities in external libraries, we will collaborate with the
proposed project {\em Extreme-scale Scientific Software Development
  Kit for the Exascale Computing Project: xSDK4ECP} to ensure that all
software developed in this proposal adheres to xSDK compliance
standards~\cite{xsdk-compliance}, including xSDK standard configure
options~\cite{xsdk-common-configure}.


\subsubsection{Related Research}
%\guidance{Identify any related research areas that would benefit from the proposed capability.}

The functionality provided by EnsembleLib is desperately needed.
Currently, parallel ensemble computations are done by using ad hoc,
nonrobust software techniques that do not even scale to the
petascale (for example, those that use files to communicate between the simulations and 
the optimization algorithms).
Research into sensitivity analysis and uncertainty
quantification methods would benefit from the EnsembleLib capability.
More advanced optimization capabilities, such as in emerging efforts
in robust optimization and mixed-integer PDE-constrained optimization,
also need EnsembleLib. Of course, improved scalability of algebraic
solvers, time integrators, and adjoint computations will benefit a wide
variety of research topics that rely on efficient solvers.
