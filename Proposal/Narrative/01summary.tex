
\section{SUMMARY}\label{sec:1}

{\bf PROJECT TITLE} \\ Preparing PETSc/TAO for Exascale


{\bf PRINCIPLE INVESTIGATOR} \\ 
Barry  Smith (Argonne National Laboratory (ANL))


% \guidance{Select one or more of the following software technology categories 
% that best describe the proposed work: Programming Models and Runtime; Tools; 
% Mathematical and Scientific Libraries and Frameworks; Data Management and 
% Workflows; Data Analytics and Visualization; System Software.}

{\bf SOFTWARE TECHNOLOGY CATEGORY} \\ Mathematical and scientific libraries and frameworks

{\bf PRIMARY FUNDING SOURCE(S)} \\
% \guidance{Specify any current and pending funding source(s) (specify agency and 
% program, duration, and amount of funding awards) currently supporting the 
% development of the proposed software technology. How does the proposed work 
% differ from that supported by other funding?}
The development of PETSc/TAO is funded through DOE's Office of Science, 
Advanced Scientific Computing Research
(ASCR) Applied Mathematics (for basic algorithmic research and
prototype software implementations) and Scientific Discovery through
Advanced Computing (SciDAC) (for software hardening, application
support, and application-specific work) programs.

The Applied Mathematics program supports research in linear solvers
(\$950K/year; next renewal cycle 9/2018), ODE integrators
(\$400K/year; next renewal cycle 9/2017), and optimization algorithms
(\$800K/year; next renewal cycle 9/2018).  The SciDAC institutes and
partnerships (being prepared for renewal now) support linear and
nonlinear solvers (\$700K/year) and optimization (\$650K/year). We
expect an increase in funding for ODE solvers and optimization in the
next SciDAC cycle, with stable funding for the algebraic solvers.  The
focus of the proposed work is on exascale algebraic solvers that
efficiently use millions to billions of degrees of concurrency and on
optimization techniques that can fully utilize a similar level of
concurrency. This requires Krylov and multigrid algorithms and
implementations that need little to no synchronization points and
that can efficiently use many-core and GPU-based compute nodes. Much
of the basic research in these areas has been performed, but it has not yet been 
funded
to be encapsulated in robust, application-hardened software that will
run at the exascale. Similarly robust software for ensemble-based
optimization at the exascale is not yet being funded by any current
projects.


%* SW: Ideally, everything in Sec 1 is on 1 page...
%* Based on my experience with ECP so far, they want this one page to send to 
%* ASCR et al.
%* This can be achieved by nuking most of the funding verbage (see FAQ) and 
%* replacing \\ with : throughout
% \newpage

\newpage

{\bf EXECUTIVE SUMMARY}
% \guidance{Provide a short description (maximum one page) of the proposed 
% software, its impact and urgency, the scope of the research and development 
% (R\&D) effort, and anticipated use of the resulting software by applications, 
% co-design centers, or other components of the software stack on future exascale 
% systems.}

Algebraic solvers (generally nonlinear solvers that use sparse linear
solvers via Newton's method) and ODE/DAE integrators form the core
computation of many numerical simulations. No scalable
``black box'' sparse solvers or integrators work for all applications, nor single implementations that work well for all scales of problem
size. Hence, algebraic solver packages provide a wide variety of
algorithms and implementations that can be customized for the
application and range of problem sizes at hand. PETSc is a widely used
software library for the scalable solution of linear, nonlinear, and
ODE/DAE systems and computation of adjoints (sometimes called
sensitivities) of ODE systems. Under {\bf Thrust 1} of our proposed work we
will focus on three topics: \emph{partially matrix-free scalable
  solvers} that can efficiently use many-core and GPU-based
systems, \emph{reduced synchronization algorithms} that can scale to
larger concurrency than solvers with synchronization points, and
\emph{performance and data structure optimizations} for all the core
data structures to better utilize many-core and GPU-based systems as
well as provide scalability to the exascale.

The availability of systems with over 100 times the processing power of
today's machines compels the utilization of these systems not just for
a single ``forward solve'' simulation (as discussed above) but rather
within a tight loop of optimization, sensitivity analysis (SA), and
uncertain quantification (UQ). This requires the implementation of a new,
scalable library for managing a dynamic hierarchical collection of
running scalable simulations, where the simulations directly feed
results into the optimization, SA, and UQ solvers. The collection of
running simulations can grow and shrink based on feedback from the
solvers.  Thus, this library must dynamically start simulations
with different parameters, resume simulations to obtain more accurate
results, prune running simulations that the solvers determine can no
longer provide useful information, monitor the progress of the
simulations and stop failed or hung simulations, and collect data
from the individual simulations both while they are running and at the
end.  This library, which we call EnsembleLib,\footnote{We use the word ``ensemble'' to refer to any collection of related simulation runs,
not just statistical ensembles.}  
should not be confused with
workflow-based scripting systems; rather it is a library that, through
the tight coupling and feedback described above, directs the multiple
concurrent ``function evaluations'' needed by optimization, SA, and UQ
solvers. Currently no library is available with any
robustness to failure of even a single simulation; meaning that current
codes must be completely restarted if a single simulation in the
ensemble crashes or hangs. This situation is clearly unacceptable at the exacale
and needs to be rectified quickly.  We refer to this work as {\bf
  Thrust 2} in the project. It will consist of two parts, the
development of EnsembleLib and the extension of TAO (our PETSc-based
scalable optimization library) with new algorithms and software to
utilize EnsembleLib.

 Nearly half of the application proposals submitted to the ECP
explicitly highlighted (in their single-slide summary) PETSc/TAO as
part of their exascale strategy, see Table 2.1.
PETSc and TAO have always been developed in close collaboration with
application teams and driven by the needs of these applications, both
within the Applied Mathematics program and through SciDAC. The
software (\href{http://www.mcs.anl.gov/petsc/publications/index.html}{http://www.mcs.anl.gov/petsc/publications/index.html}) is widely used in applications within both DOE and the wider
community. Several
Gordon Bell prize winning codes have utilized PETSc. In 2015 the PETSc
development team won the SIAM/ACM Prize in Computational Science and
Engineering; the first time a software development team has won the
award.
 

 
   
 
