\clearpage
\section{EXASCALE SOFTWARE CHALLENGE}\label{sec:2}

\subsection{EXASCALE TARGET PROBLEM\label{sec:2.1}}
%% \guidance{Briefly describe the specific research and development results that 
%% will help applications and/or the leadership facility software infrastructure to 
%% productively use and manage the projected exascale systems and their associated 
%% workflows. Clearly state the problems and requirements that are addressed by 
%% this software technology at either the application level or within the overall 
%% software stack. What challenges need to be overcome to enable the software to 
%% run efficiently/effectively on an exascale system?}

PETSc/TAO, \href{http://www.mcs.anl.gov/petsc}{http://www.mcs.anl.gov/petsc}, is a software library for the 
scalable solution of linear, nonlinear, and ODE/DAE systems,
 computation of adjoints (sometimes called sensitivities) of ODE
 systems, and optimization. We propose to develop and extend PETSc/TAO
 in two crucial directions: simulations (that require algebraic
 solvers and/or time integrators) at the exascale and optimization at
 the exascale utilizing coupled ensembled simulations.

 \emph{\bf Thrust 1: Libraries for simulation at the exascale}

   A key difficulty for proposed exascale systems is that traditional
 ``sparse-matrix-based'' techniques for linear, nonlinear, and ODE
 solvers, as well as optimization algorithms, are memory-bandwidth
 limited. In other words, the ratio of floating-point operations to
 memory access is low. A by-product of this is the
 efficiently utilizing SIMD and GPU floating-point
 units is difficult. Fortunately, ``matrix-free'' solvers provide a way
 forward. The terminology is misleading, however, because these are
 not actually matrix-free (in fact, the use of sparse matrices for a
 portion of the solver computations is crucial); therefore, they should be called
 ``partially matrix-free solvers.'' In this approach the
 discretizations are done by using high-order methods, which have much
 higher floating-point-to-memory-access ratios (and good potential use
 of SIMD and GPUs); the sparse matrices are needed only within coarser
 levels of the discretization within the preconditioner. This
 technique was used by the 2015 Gordon Bell prize-winning
 paper \cite{gb}, which actually utilized PETSc, one of the few times a simulation based on implicit
 solvers has won the award. The \emph{partially matrix-free scalable
 solvers} subproject of Thrust 1 focuses on and extends this work. This work is
 coupled to the proposed ECP co-design Center for
 Efficient Exascale Discretizations (CEED), which focuses on high-order
 methods (the PI of this proposal is on that proposal as well).

Another difficulty for proposed exascale systems is that any
synchronizations required across all compute units---for example, an
inner product or a norm---can dramatically affect the scaling of the
solvers. Traditional Krylov solvers such as the conjugate gradient
method and GMRES have one or more synchronizations per cycle. Recent
basic research in iterative solvers has resulted in pipelined Krylov
methods that delay the use of the results of inner products and norms,
thus allowing overlapping of the reductions and other computation. We
have implemented several in PETSc, but they require much more work before
being ready for exascale systems. The \emph{reduced synchronization algorithms}
subproject of Thrust 1 focuses on this work.
 
 The programming model for PETSc/TAO on multi/many-core systems is based on a 
 thread neutral ``bypass MPI''
 approach, which implements default interprocessor communication using MPI but
 bypasses the use of MPI in performance-critical regions for higher
 performance and thereby maintains MPI portability. For example, we have
 already developed prototype code that seamlessly performs ``in-node''
 communication via shared memory and ``across-node'' communication via
 MPI for the ghost point updates in PETSc. Other specific portions of the current code that require in-node optimizations are
 sparse matrix-matrix products needed by algebraic multigrid, computation of independent sets and other graph operations such as colorings, triangular solves, shared memory relaxations, and better utilization of wide SIMD units. An advantage of the ``bypass MPI'' approach is that one can determine
 the bottlenecks in MPI performance on real applications by profiling
 and then ``fix them'' one at a time. The third subproject in Thrust 1 focuses 
on these \emph{node-level optimizations} as well as appropriate 
\emph{GPU kernels for IBM/Nvidia systems}.

\newpage

\emph{\bf Thrust 2: Libraries for optimization, sensitivity analysis
   (SA), and uncertainty quantification (UQ) at the exascale}  
   
 Availability of systems with over 100 times the processing
 power of today's machines compels the utilization of these systems
 not just for a single ``forward solve'' simulation
 but rather within a tight loop of optimization, SA, and UQ. This
 requires implementation of a new, scalable library for managing a
 dynamic hierarchical 
  % \todo{Why hierarchical?} 
 % Not sure that it's worth getting into here, but hierarchies include
 % * sim - local opt solver - global opt solver
 % * collection of stoch params evaluated for each of collection of det 
 %   params from each of several local runs generated within a MOO solver
 collection of running 
 scalable simulations, where the simulations directly feed results
 into the optimization, SA, and UQ solvers. The collection of
 running simulations can grow and shrink based on feedback 
 from the solvers.
 Thus, this library must be able to dynamically start simulations with different
 parameters, resume simulations to obtain more accurate results,
 prune running simulations that the solvers determine can no longer 
 provide useful information, monitor the progress of the simulations,
 and stop failed or hung simulations, and collect data from the 
 individual simulations both while they are running and at the
 end.  This library, which we
 call EnsembleLib,
%  \footnote{We use the word ensemble 
%  to refer to any collection of related
%  simulation runs, not just statistical ensembles.}
should not
 be confused with workflow-based scripting systems; rather it is a
 library that, through the tight coupling and feedback described
 above, directs the multiple concurrent ``function evaluations''
 needed by optimization, SA, and UQ solvers. Currently no library is available with any robustness to failure of a
 single simulation. Current codes must be completely
 restarted if a single simulation in the ensemble crashes or hangs.
 This situation is clearly unacceptable at the exascale and needs to be 
rectified
 quickly.

The capabilities provided by EnsembleLib extend to many higher-level 
design, decision, and control problems 
that HPC applications are actively solving today or have thus far neglected 
(e.g., because of a lack of tools or computational expense).  These problems 
include multiobjective optimization, whereby Pareto-optimal tradeoffs 
between two or more objectives are sought \cite{Ehrgo00b}; forward uncertainty 
propagation and correlation analysis, which are vital for assessing
parameter dependencies and basic uncertainty quantification \cite{NAP13395}; 
and branch-and-bound approaches for problems with mixtures of continuous and 
integer variables \cite{LeeLeyffer:12}. 
We will demonstrate the new capabilities provided by EnsembleLib on two 
exascale-ready subprojects:
\begin{enumerate}
 \item Computation of multiple local minimizers and the associated local 
sensitivities.
 \item Stochastic optimization.
\end{enumerate}
These problems and our development strategy are expanded on in 
\Sec{sec:softdev}.





\subsection{IMPACT AND URGENCY\label{sec:2.2}}
%\guidance{What is the potential impact of the successful completion of the 
%proposed work? Explain the importance of carrying out this work within the next 
%few years.}

Nearly half of the application proposals submitted to the ECP
explicitly highlighted (in their single-slide summary) PETSc/TAO as
part of their exascale strategy. These applications span national
strategic initiatives and lead institutions; see
Table~\ref{tab:apps}. 

The needs of these applications span the thrusts and subprojects addressed in 
this proposal and come from both long-time users of PETSc/TAO and 
new use cases enabled at the exascale. 
The application requirements include needs for PETSc solvers
(e.g., in the \textit{Cloud-Resolving Climate Modeling of the
Earth's Water Cycle} and \textit{Coupled Monte Carlo Neutronics and Fluid
Flow Simulation of Small Modular Reactors} projects);
TAO's optimization and model calibration tools 
(e.g., in the \textit{NuEx} and \textit{Exascope} projects); and more 
exploratory interest in/studies with our proposed ensemble capabilities (e.g., 
in the \textit{Multiscale Coupled Urban Systems} project). 
Furthermore, we have engaged in a number
of additional discussions with exascale applications not reflected in their 
single-slide summaries. We expect that such uses (e.g., an exascale capability 
to compute multiple high-quality minimizers for halo finding in the 
\textit{Computing the Sky at Extreme Scales} project) will be refined and 
developed as the ECP application, co-design, and software project awards 
are made.



\begin{table}[t]
\centering
\caption{ECP application proposals requiring PETSc/TAO.}\label{tab:apps}
\small
\vspace{-1ex}
\begin{tabular}{| p{2.25in} | p{2.6in} | p{1.25in} |}\hline
{\bf National Strategy} & {\bf Project} &  {\bf PI} \\ \hline \hline
% % Salman said he wants us via email but not in his summary slide: 
% Particle Physics Project Prioritization Panel (P5)
% & Computing the Sky at Extreme Scales
% &
% \\ \hline
Energy-Water Nexus; 
Smart Cities Initiative
&
Multiscale Coupled Urban Systems
& Charles Catlett (ANL)
\\ \hline
MGI; 
New Energy and Transportation System Technologies
&
New Science at Exascale-enabled Light Sources: The Exascope
& Ian Foster (ANL)
\\ \hline 
2015 Long Range Plan for Nuclear Science;
FRIB, DUNE, Majorana, \& Exo Experiments
&
Nuclei at Exascale for Next-Generation Experiments (NuEx)
& Joseph Carlson (LANL)
\\ \hline 
EERE FORGE;
FE NRAP;
Energy-Water Nexus;
SubTER Crosscut
&
An Exascale Subsurface Simulator of Coupled Flow, Transport, Reactions and 
Mechanics
& Carl Steefel (LBNL)
\\ \hline 
Climate Action Plan;
2020 Greenhouse Gas Emission Goals;
2030 Carbon Reduction Goals;
Sunshot Initiative
&
Performance Prediction of Multiphase Energy Conversion Devices with Discrete 
Element, PIC, and Two-Fluid Models (MFIX-Exa)
& Madhava Syamlal (NETL)
\\ \hline 
Climate Action Plan;
SMR Licensing Technical Support;
Gateway for Accelerated Innovation in Nuclear
&
Coupled Monte Carlo Neutronics and Fluid Flow Simulation of Small Modular 
Reactors
& Thomas Evans (ORNL)
\\ \hline 
Cancer Moonshot;
BRAIN Initiative;
Climate Action Plan
&
The Combinatorial Applications for Systems Biology Analysis (CASBA) Platform
& Daniel Jacobson (ORNL)
\\ \hline
Grid Modernization Initiative; 
Climate Action Plan
&
Optimizing Stochastic Grid Dynamics at Exascale
& Zhenyu (Henry) Huang (PNNL)
\\ \hline 
ITER; 
Fusion Experiments: NSTX, DIII-D, Alcator C-Mod
&
High-Fidelity Whole Device Modeling of Magnetically Confined Fusion Plasmas
& Amitava Bhattacharjee (PPPL)
\\ \hline 
2020 Greenhouse Gas Emission Goals;
2030 Carbon Reduction Goals
&
Transforming Combustion Science and Technology with Exascale Simulations
& Jacqueline Chen (SNL)
\\ \hline 
Climate Action Plan
&
Cloud-Resolving Climate Modeling of the Earth's Water Cycle
& Mark Taylor (SNL)
\\ \hline 
% The following mentions "TAO" but we need to find out if actually PETSc's TAO:
MGI; 
Climate Action Plan
&
NWChemEx: Tackling Chemical, Materials and Biomolecular Challenges in the 
Exascale Era
& Thom Dunning (PNNL)
\\ \hline
\end{tabular}
\end{table}


The solver software development and tuning will take
several years to become exascale application-ready.
If this work is delayed, the applications ability to perform new science will similarly be delayed.


